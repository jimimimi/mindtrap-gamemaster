const redis = require('../routes/helper/redis').redis;
const Rooms = require('../routes/data/rooms');

async function migrate() {
    let rooms = await Rooms.get_rooms();
    for (let i = 0; i < rooms.length; i++) {
        const room = rooms[i];
        let keys = await redis.keysAsync(`data:sessions:${room.id}:*`);
        for (let j = 0; j < keys.length; j++) {
            const key = keys[j];
            let session = await redis.hgetallAsync(key);
            try {
                await redis.hmsetAsync(`data:sessions:${session.id}`, session);
                await redis.delAsync(key);
            } catch (error) {
                console.error(error);
                console.error(error.stack);
            }
        }
    }
}

migrate();