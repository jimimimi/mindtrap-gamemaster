/**
 * Module contains all cron jobs that run on server
 */

const schedule = require('node-schedule');
const Sessions = require('../data/sessions');
const Rooms = require('../data/rooms');
const config = require('../../config/appconfig');
const moment = require('moment');
const axios = require('axios');
const debug = require('debug')('mindtrap-local:cron');

/**
 * Used to monitor active sessions
 * and server will stop all sessions that run out of time
 * @param {Number} date unix timestamp
 */
function session_monitor(date) {
    debug("Cron Task Run!");
    Sessions.get_all_active_sessions().then(function(sessions) {
        sessions.forEach(session => {
            // If session is paused we cannot kill it
            if (session.state == 'PAUSED') {
                return;
            }

            let three_minutes = 3 * 60;

            // Here we calculate the time the session is supposed to stop
            let end_date;
            if (session.stop_date != 'false') {
                end_date = moment.utc(session.stop_date);
            } else {
                end_date = moment.utc(session.start_date).add(session.total_seconds, 'seconds').add(session.extra_seconds, 'seconds').add(session.paused_seconds, 'seconds');
            }

            let end_time = end_date.diff(moment.utc(), 'seconds');

            debug('stop-time', end_time);

            // Here we determine if we force stop the session or not
            // We force stop sessions that were supposed to stop more than 3 minutes
            if ( end_time < -three_minutes ) {
                debug('Force stop session:', session.id);
                Sessions.session_stop(session.room_id, false, true);
            } else if ( end_time < 0 ) {
                debug('Stop session:', session.id, session.state);
                Sessions.session_stop(session.room_id, false, false);
            }
        });
    });
}

session_monitor();
// RUN EVERY 15 seconds
let session_job = schedule.scheduleJob('*/15 * * * * *', session_monitor);

/**
 * Used to monitor the syncronization of archived sessions back to the cloud server
 * After it synchronized the sessions it deletes them from the local database
 * @param {Number} date unix timestamp
 */
async function sync_archived_sessions_monitor(date) {
    let rooms = await Rooms.get_rooms();

    try {
        let sessions = await Sessions.get_archived_sessions();
        let response = await axios.post(config.server_sync_url, sessions);
        for (let i = 0; i < sessions.length; i++) {
            const session = sessions[i];
            Sessions.delete_archived_session(session.id);
        }
    } catch (error) {
        console.error('[SYNC][SESSIONS] Could not connect to remote server!', error.message);
    }
}

sync_archived_sessions_monitor();
let sync_archived_sessions_job = schedule.scheduleJob(config.cron.sync_history, sync_archived_sessions_monitor);

/**
 * Used to monitor the syncronization of the rooms state the the cloud server
 * @param {Number} date unix timestamp
 */
async function sync_rooms_active_sessions_monitor(date) {
    let rooms = await Rooms.get_rooms();

    let data = [];
    for (let i = 0; i < rooms.length; i++) {
        const room = rooms[i];

        data.push({
            id: room.id,
            session: (room.session) ? room.session.start_date : false
        });

    }

    try {
        await axios.post(config.server_rooms_sync_url, data);
    } catch (error) {
        console.error('[SYNC][ROOMS] Could not connect to remote server!', error.message);
    }
}

sync_rooms_active_sessions_monitor();
let sync_rooms_active_sessions_job = schedule.scheduleJob(config.cron.sync_room_active_sessions, sync_rooms_active_sessions_monitor);
