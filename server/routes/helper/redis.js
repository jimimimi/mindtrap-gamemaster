const config = require('../../config/appconfig');
const bluebird = require('bluebird');
const redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

let redisClient = redis.createClient(config.redis);
let subClient = redis.createClient(config.redis);

function send_event_helper(channel, evt, data = {}) {
    redisClient.publish(channel, JSON.stringify({
        event: evt,
        data: data
    }));
}

exports.redis = redisClient;
exports.subClient = subClient;
exports.send_event_helper = send_event_helper;