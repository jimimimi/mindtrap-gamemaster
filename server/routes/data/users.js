const redis = require('../helper/redis').redis;

function get_user(username) {
    return redis.hgetallAsync('config:users:' + username);
}

module.exports = {
    get_user
}