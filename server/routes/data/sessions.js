/**
 * Module used to manage game sessions on the database
 */

const redis = require('../helper/redis').redis;
const send_event_helper = require('../helper/redis').send_event_helper;
const Promise = require("bluebird");
const moment = require('moment');
const HistoryLog = require('./history-log');
const debug = require('debug')('mindtrap-local:data:sessions');

/**
 * Used to get active session of specific @room_id
 * @param {Number} room_id room id you want to get active session
 * @return {Promise<Object> / Promise<Boolean>}
 */
function get_active_session(room_id) {
    return redis.hgetallAsync('data:active_sessions:' + room_id).then((session) => {
        if (session) {
            session.channel_volumes = session.channel_volumes.split(',').map(parseFloat);
        }
        return session;
    })
}

/**
 * Used to get all active sessions
 * @return {Promise<Array>}
 */
function get_all_active_sessions() {
    return redis.keysAsync('data:active_sessions:*')
        .then(function (keys) {
            let promises = [];

            keys.forEach(key => {
                promises.push(redis.hgetallAsync(key));
            });

            return Promise.all(promises);
        });
}

// Used to keep the clear session timers for the rooms
let stop_timers = {};

/**
 * Used to clear and archive active session from specified @room_id
 * @param {Number} room_id the room you want to clear and archive the active session
 */
async function clear_and_archive(room_id) {
    try {
        // GET active session
        let session = await get_active_session(room_id);
        // ARCHIVE active session
        await redis.hmsetAsync(`data:sessions:${session.id}`, session);
        // DELETE active session
        await redis.delAsync(`data:active_sessions:${room_id}`);
        // DELETE stop timer for room
        delete stop_timers[room_id];

        send_event_helper('operator.0', 'session-clear', {
            room_id
        });
        send_event_helper('operator.' + room_id, 'session-clear');
        send_event_helper('client.' + room_id, 'session-clear');
    } catch (error) {
        debug('Error', error);
        throw error;
    }
}

/**
 * Used to stop active session from specified @room_id
 * @param {Number} room_id the room you want to stop the active session
 * @param {Boolean} escaped true if the team escaped the room
 * @param {Boolean} force force stop room, this means that the reset timer will not start
 * @param {Number} user_id the user who invoked the session_stop action, in order to record it
 * @return {Promise<Object>}
 */
function session_stop(room_id, escaped, force, user_id) {
    debug('Session stop', room_id, escaped, force);

    // Check if room is stopped and waiting to be cleared and archived
    if (room_id in stop_timers) {
        debug('room_id', room_id, 'in stop queue');
        if (force) {
            clear_and_archive(room_id);
            return true;
        } else {
            return false;
        }
    }

    return stop(room_id, escaped).then(function (session) {
        // If team escaped send create log and update operator log
        if (escaped) {
            let escape_log = {
                user_id,
                action: 'ESCAPED',
                date: moment.utc().format()
            }
            send_event_helper('operator.' + room_id, 'log', escape_log);

            HistoryLog.create_for_session(session.id, escape_log);
        }

        let log = {
            user_id,
            action: 'STOP',
            date: moment.utc().format()
        }

        HistoryLog.create_for_session(session.id, log);

        send_event_helper('operator.' + room_id, 'log', log);
        send_event_helper('operator.' + room_id, 'session-stop', {
            session,
            escaped
        });
        send_event_helper('client.' + room_id, 'session-stop', {
            session,
            escaped
        });
        // Inform global operators channel
        send_event_helper('operator.0', 'session-stop', {
            room_id,
            session,
            escaped
        });


        // If session_stop is forced => clear_and_archive session
        if (force) {
            clear_and_archive(room_id);
        } else {
            stop_timers[room_id] = true;
        }

        return {
            status: 'OK'
        };
    });
}

/**
 * Used to SAVE!!! active session for specified @room_id
 * @param {Number} room_id the room id you want to save the session @data
 * @param {Object} data the data payload for the session
 */
function create_or_modify(room_id, data) {
    data.channel_volumes = data.channel_volumes.toString();
    return redis.hmset('data:active_sessions:' + room_id, data);
}

/**
 * Used transit session to stopped state 'ESCAPED' : 'NOT-ESCAPED'
 * @param {Number} room_id the room you want to stop the session
 * @param {Boolean} escaped true if the team escaped the room
 * @return {Promise<object>}
 */
function stop(room_id, escaped) {
    return get_active_session(room_id).then(function (session) {
        if (session.state == 'RUNNING') {
            session.stop_date = moment.utc().format();
            session.state = escaped ? 'ESCAPED' : 'NOT-ESCAPED';
            create_or_modify(room_id, session);
        }
        return session;
    });
}

/**
 * Used to update active session information
 * @param {Number} room_id room you want to update active session
 * @param {Object} payload session information
 */
function update_active_session(room_id, payload) {
    return get_active_session(room_id).
    then(function (session) {
        let keys = Object.keys(payload);
        for (var i = 0; i < keys.length; i++) {
            let key = keys[i];
            let configuration = payload[key];
            if ('$add' in configuration) {
                session[key] = parseInt(session[key]) + parseInt(configuration['$add']);
            } else if ('$set' in configuration) {
                session[key] = configuration['$set'];
            }
        }
        create_or_modify(room_id, session);
        return session;
    });
}

/**
 * Used to pause room active session
 * @param {Number} room_id the room you want to pause the time
 * @param {Number} user_id the user who invoked the function
 */
function pause(room_id, user_id) {
    return get_active_session(room_id).
    then(function (session) {
        if (session.state != 'RUNNING') {
            throw {
                message: 'State is not running!'
            };
        }

        let log = {
            user_id,
            date: moment.utc().format(),
            action: 'PAUSED'
        };

        session.paused_date = moment.utc().format();
        session.state = 'PAUSED';
        if (session.help_date != 'false') {
            session.help_seconds = moment.utc(session.help_date).diff(moment.utc(), 'seconds');
            session.help_date = false;
        }
        create_or_modify(room_id, session);

        HistoryLog.create(room_id, log);
        send_event_helper('operator.0', 'session-paused', {
            room_id,
            session
        });
        send_event_helper('operator.' + room_id, 'session-paused', session);
        send_event_helper('client.' + room_id, 'session-paused', session);
        send_event_helper('operator.' + room_id, 'log', log);

        return session;
    });
}

/**
 * Used to resume room active session
 * @param {Number} room_id the room you want to resume the time
 * @param {Number} user_id the user who invoked the function
 */
function unpause(room_id, user_id) {
    return get_active_session(room_id).
    then(function (session) {
        if (session.state != 'PAUSED') {
            throw {
                message: 'State is not paused!'
            };
        }

        let paused_seconds = moment.utc().diff(moment.utc(session.paused_date), 'seconds');

        let log = {
            user_id,
            date: moment.utc().format(),
            action: 'UNPAUSED',
            text: paused_seconds
        };
        if (session.help_seconds != 'false') {
            session.help_date = moment.utc().add(session.help_seconds, 'seconds').format();
            session.help_seconds = false;
        }
        session.paused_seconds = parseInt(session.paused_seconds) + paused_seconds;
        session.paused_date = false;
        session.state = 'RUNNING';

        HistoryLog.create(room_id, log);
        send_event_helper('operator.0', 'session-unpaused', {
            room_id,
            session
        });
        send_event_helper('operator.' + room_id, 'session-unpaused', session);
        send_event_helper('client.' + room_id, 'session-unpaused', session);
        send_event_helper('operator.' + room_id, 'log', log);

        create_or_modify(room_id, session);
        return session;
    });
}

/**
 * Used to fetch archive sessions in order to sync them with the cloud application
 * @param {Number} limit how many archived sessions you want to retrieve
 */
async function get_archived_sessions(limit = 20) {
    let keys = await redis.keysAsync('data:sessions:*');
    let sessions = [];

    // Limit archived sessions
    if (keys.length < limit) {
        limit = keys.length;
    }

    for (let i = 0; i < limit; i++) {
        const key = keys[i];
        let session = await redis.hgetallAsync(key);
        // REMOVE unused fields
        delete session.paused_date;

        delete session.help_requested;
        delete session.help_date;
        delete session.help_seconds;

        delete session.background_music;
        delete session.master_volume;
        delete session.channel_volumes;
        delete session.help_seconds;

        session.history_log = [];
        let history_keys = await redis.keysAsync('data:history_log:' + session.id + ':*');
        for (let j = 0; j < history_keys.length; j++) {
            const history_key = history_keys[j];
            let log = await redis.hgetallAsync(history_key);
            session.history_log.push(log);
        }
        sessions.push(session);
    }

    return sessions;
}

/**
 * Used to delete archived sessions from database
 * @param {uuidv4} session_id session id you want to delete from database
 */
async function delete_archived_session(session_id) {
    await redis.delAsync(`data:sessions:${session_id}`);
}

module.exports = {
    start: create_or_modify,
    stop,
    get_active_session,
    get_all_active_sessions,
    get_archived_sessions,
    delete_archived_session,
    session_stop,
    update_active_session,
    clear_and_archive,
    pause,
    unpause
}
