/**
 * Module used to manage the rooms data of the database
 */
const redis = require('../helper/redis').redis;
const Promise = require("bluebird");
const util = require('util');
const recursive = util.promisify(require("recursive-readdir"));
const Sessions = require('./sessions');
const HistoryLog = require('./history-log');
const debug = require('debug')('mindtrap-local:data:rooms');

/**
 * Function used to fetch room using the redis key
 * @param {String} key redis key location to fetch room
 * @return {Promise}
 */
async function get_room_by_key(key) {
    try {
        let room = await redis.hgetallAsync(key);

        // Parse room properties
        room.walkthroughs = JSON.parse(room.walkthroughs);
        room.texts = JSON.parse(room.texts);
        room.clients = JSON.parse(room.clients);

        // Load room active session
        let session = await Sessions.get_active_session(room.id);
        if (session) {
            room.session = session;
            // Load active session history logs
            let logs = await HistoryLog.fetch_from_session(session.id);
            room.session.history_log = logs;
        } else {
            room.session = false;
        }

        // Load room assets
        room.assets = {};
        const assets = ['images', 'music', 'sound', 'videos'];
        let promises = [];

        for (var i = 0; i < assets.length; i++) {
            let asset = assets[i];
            promises.push(
                recursive(`./public/files/${room.name}/${asset}/`)
                .then((files) => {
                    let re = /\\/g;
                    files = files.map(file => file.replace(`public\\files\\${room.name}\\${asset}\\`, '').replace(re, '/'));
                    files = files.filter(file => !file.includes('thumbs'));
                    room.assets[asset] = files;
                    return;
                })
                .catch((err) => {
                    if (err.code == 'ENOENT') {
                        files = [];
                        debug('Asset directory is missing:', err.path);
                    } else {
                        throw err;
                    }
                })
            );
        }

        // Get room clients connection status
        for (var slot = 1; slot <= 4; slot++) {
            let temp_slot = slot;
            promises.push(redis.hgetallAsync(`channels:clients:connections:${room.id}:${slot}`).then(function (connection) {
                if (temp_slot in room.clients) {
                    if (connection && connection.connected == 'YES') {
                        room.clients[temp_slot].connected = true;
                    } else {
                        room.clients[temp_slot].connected = false;
                    }
                }
            }).catch(function (err) {
                throw err;
            }));
        }

        await Promise.all(promises);
        return room;

    } catch (error) {
        throw error;
    }
}

/**
 * Function used to get specific room information
 * @param {Number} id
 * @return {Promise}
 */
function get_room_by_id(id) {
    return get_room_by_key('config:rooms:' + id);
}

/**
 * Used to get all the rooms information from the database
 */
async function get_rooms() {
    try {
        let keys = await redis.keysAsync('config:rooms:*')
        let promises = [];

        for (var i = 0; i < keys.length; i++) {
            let key = keys[i];
            promises.push(get_room_by_key(key));
        }

        let rooms = await Promise.all(promises);

        return rooms;
    } catch (error) {
        throw error;
    }
}

module.exports = {
    get_room_by_key,
    get_room_by_id,
    get_rooms
}
