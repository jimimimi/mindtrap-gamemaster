/**
 * Module used to manage history-log reconds of sessions
 */
const redis = require('../helper/redis').redis;
const Promise = require("bluebird");

/**
 * Used to add a history log record for and active session of @room_id
 * @param {Number} room_id the room you want to get the active session
 * @param {Object} payload the history log record payload
 */
function create(room_id, payload) {
    return redis.hgetallAsync('data:active_sessions:' + room_id)
        .then(function (session) {
            return create_for_session(session.id, payload);
        });
}

/**
 * Used to add a history log record for specific @session_id
 * @param {uuidv4} session_id session you want to add a history log record
 * @param {Object} payload the history log record payload
 */
function create_for_session(session_id, payload) {
    payload.session_id = session_id;
    return redis.keysAsync(`data:history_log:${session_id}:*`)
        .then(function (keys) {
            let key = keys.length;
            return redis.hmset(`data:history_log:${payload.session_id}:${key}`, payload);
        }).then(function () {
            return payload;
        })
}

/**
 * Used to fetch all history log records for active session of @room_id
 * @param {Number} room_id the room id you want to fetch for the active
 * session the history log records
 */
function fetch(room_id) {
    return Session.get_active_session(room_id)
        .then(fetch_from_session);
}

/**
 * Used to fetch all history log records for specific @session_id
 * @param {uuidv4} session_id the session id you want to fetch all
 * the history log records
 */
function fetch_from_session(session_id) {
    return redis.keysAsync(`data:history_log:${session_id}:*`)
        .then(function (keys) {
            let logs = [];
            let promises = [];

            for (var i = 0; i < keys.length; i++) {
                let key = keys[i];
                let promise = redis.hgetallAsync(key).then(function (log) {
                    return log;
                });
                promises.push(promise);
            }
            return Promise.all(promises);
        });
}

module.exports = {
    create,
    create_for_session,
    fetch,
    fetch_from_session
}
