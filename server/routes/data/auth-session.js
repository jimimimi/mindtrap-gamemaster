const redis = require('../helper/redis').redis;

/**
 * Module used to manage logged in users sessions
 */

/**
 * Save @sid session @payload to database
 * Sessions expire after 1 day
 * @param {uuidv4} sid session id want to save
 * @param {object} payload payload to save in database
 * @return {promise}
 */
function save(sid, payload) {
    return redis.setAsync('sess:' + sid, JSON.stringify(payload), 'EX', 24 * 60 * 60);
}

/**
 * Used to destroy a session from the database
 * @param {uuidv4} sid session id want to destroy
 * @return {promise}
 */
function destroy(sid) {
    return redis.delAsync('sess:' + sid);
}

/**
 * Used to load session from database
 * @param {uuidv4} sid session id want to destroy
 * @return {promise}
 */
function load(sid) {
    // Refresh session to expire after 1 day
    redis.expire('sess:' + sid, 24 * 60 * 60);
    return redis.getAsync('sess:' + sid).then((data) => {
        return JSON.parse(data)
    });
}

module.exports = {
    save,
    destroy,
    load
}
