const express = require('express');
const router = express.Router();
const axios = require('axios');
const config = require('../config/appconfig');
const redis = require('./helper/redis').redis;
const send_event_helper = require('./helper/redis').send_event_helper;
const Promise = require("bluebird");
const moment = require('moment');
const uuidv4 = require('uuid/v4');
const Rooms = require('./data/rooms');
const Sessions = require('./data/sessions');
const Users = require('./data/users');
const HistoryLog = require('./data/history-log');
const AuthSession = require('./data/auth-session');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const environment = process.env.NODE_ENV || 'development';

function log_error(error) {
    console.error(error);
    console.error(error.stack);
}

/**
 * Unprotected routes
 */
router.get('/login', function(req, res, next) {
    res.render('login', { title: 'Login Page' });
});

router.post('/login', async function(req, res, next) {
    if ( ! req.body.username) {
        return res.status(403).json({ message: '"username" field is missing' });
    }

    if ( ! req.body.password) {
        return res.status(403).json({ message: '"password" field is missing' });
    }

    let user;
    try {
        user = await Users.get_user(req.body.username);
    } catch (error) {
        log_error(error);
        return res.status(403).json({ message: 'Invalid username.' });
    }

    if (user === null) {
        return res.status(403).json({ message: 'Invalid username.' });
    }

    let user_password = user.password;
    delete user.password;

    bcrypt.compare(req.body.password, user_password, async function(err, result) {
        if (err) {
            log_error(err);
            return res.status(500).json({message: 'Internal server error #2.22-1816'});
        }

        if (result) {
            let session_id = uuidv4();
            let token = jwt.sign({ sid: session_id }, config.secret);
            try {
                await AuthSession.save(session_id, {user});
            } catch (error) {
                log_error(error);
                return res.status(500).json({message: 'Internal server error #2.22'});
            }
            return res.json({token});
        } else {
            return res.status(401).json({message: 'Invalid password'});
        }
    });
});

/**
 * Commented section is JWT authentication middleware
 * Uncomment it if you want to verify JWT tokens
 */

// let auth_middleware = async function(req, res, next) {
//     if (req.headers && req.headers.authorization) {
//         let payload = jwt.verify(req.headers.authorization, config.secret);
//         // TRY CATCH
//         let session = null;
//         try {
//             session = await AuthSession.load(payload.sid);
//         } catch (error) {
//             console.log(error);
//         }
//         if (session == null) {
//             return res.status(403).json({message: 'Session expired!', status: 403});
//         }
//         req.sessionID = payload.sid;
//         req.session = session;
//         return next();
//     } else {
//         return res.status(401).json({message: 'Permission Denied!', status: 401});
//     }
// };

/**
 * Dummy auth middleware
 */
let auth_middleware = function (req, res, next) {
    let session = {
        "user": {
            "id": "-1",
            "active": "1",
            "store_id": "-1",
            "gamemaster": "1",
            "name": "Anonymous",
            "email": "anonymous@example.com",
            "username": "anonymous",
            "store": "anonymous",
            "storetype": "store"
        }
    }

    req.sessionID = '45fc70dd-79db-47d9-93fd-cfa28e49bb51'
    req.session = session
    next()
}

router.get('/user-info', auth_middleware, async function (req, res, next) {
    return res.json(req.session);
});

router.post('/logout', auth_middleware, async function(req, res, next) {
    try {
        await AuthSession.destroy(req.sessionID);
        return res.send();
    } catch (error) {
        return res.status(403).json({message: 'Session expired!', status: 403});
    }
});

/**
 * Used to display all the rooms data and state
 */
router.get('/data', function (req, res, next) {
    redis.keysAsync('config:rooms:*').then(function (keys) {
        let promises = [];
        let rooms = [];

        for (var i = 0; i < keys.length; i++) {
            let key = keys[i];
            promises.push(Rooms.get_room_by_key(key));
        }

        Promise.all(promises).then(function (rooms) {
            res.json(rooms);
        });

    }).catch(function (err) {
        res.status(500).json({
            message: "Could not fetch application data"
        });
        throw err;
    });
});

/**
 * Used to get a single room data and state
 */
router.get('/data/room/:room_id', function (req, res, next) {
    let room_id = req.params.room_id;

    Rooms.get_room_by_id(room_id).then(function (room) {
        res.json(room)
    }).catch(function (err) {
        // TODO log errors
        res.status(404).json({
            message: "Room not found."
        })
    });
})

function replaceAt(string, index, replace) {
    return string.substring(0, index) + replace + string.substring(index + 1);
}

/**
 * Used to sync database data from master server
 */
router.get('/sync', function(req, res, next) {
    redis.del('data');

    axios.get(config.sync_url).then(function (response) {
        const data = response.data;
        const users = data.users;
        const rooms = data.rooms;
        for (var i = 0; i < users.length; i++) {
            let user = users[i];
            if (user.active == 1) {
                user.password = replaceAt(user.password, 2, 'a');
                redis.hmset('config:users:' + user.username, user);
            }
        }

        for (var i = 0; i < rooms.length; i++) {
            let room = rooms[i];
            if (room.active == 1) {

                let active_clients = {};

                for (var client_id = 1; client_id <= 4; client_id++) {
                    let slot = client_id;
                    if (client_id == 1 || room[`client${client_id}_enable`] == 1) {
                        active_clients[slot] ={
                            ip: room[`client${client_id}_ip`]
                        };
                        redis.hmset('channels:clients:ips:' + room[`client${client_id}_ip`], {
                            id: room.id,
                            slot: client_id
                        })
                    }
                }

                redis.hmset('config:rooms:' + room.id, {
                    id: room.id,
                    active: 1,
                    name: room.name,
                    description: room.description,
                    duration: room.duration,
                    hint_duration: room.hint_duration,
                    audio_out: room.audio_out,
                    walkthroughs: JSON.stringify(room.walkthroughs),
                    texts: JSON.stringify(room.texts),
                    clients: JSON.stringify(active_clients),
                });
            }
        }

        res.json(data);
    }).catch(function (err) {
        res.status(500).json({
            message: 'Error while syncing local database.'
        });
        log_error(err);
        throw err;
    })
});

/**
 * Used to drive client to spesific room-client configuration
 */
router.get('/roomclient', function (req, res, text) {
    let ip = req.clientIp.replace('::ffff:', '');
    res.redirect(`/#/client/${ip}`);
});

/**
 * Used to start a session for room
 */
router.post('/room/:room_id/session/start', auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;

    redis.hgetallAsync('config:rooms:' + room_id).then(function (room) {

            let session = {
                id: uuidv4(),
                start_date: moment.utc().format(),
                stop_date: false,
                paused_date: false,
                state: 'RUNNING', // [RUNNING, PAUSED, ESCAPED, NOT-ESCAPED]
                user_id: req.session.user.id,
                room_id: room_id,
                total_seconds: room.duration * 60,
                help_requested: false,
                help_date: false, // used to hold time the next available hint
                help_seconds: false, // used to keep the seconds until next help when session is paused
                background_music: 'background.mp3', // TODO modify background_music state
                master_volume: 1,
                channel_volumes: [1, 1, 1],
                walkthrough: 0,
                extra_seconds: 0,
                paused_seconds: 0
            };

            let log = {
                user_id: req.session.user.id,
                action: 'START',
                date: moment.utc().format()
            }

            HistoryLog.create_for_session(session.id, log);

            Sessions.start(room_id, Object.assign({}, session));

            send_event_helper('operator.' + room_id, 'session-start', session);
            send_event_helper('operator.0', 'session-start', {room_id, session});
            send_event_helper('operator.' + room_id, 'log', log);
            send_event_helper('client.' + room_id, 'session-start', session);

            res.json(session);
    }).catch(function (err) {
        log_error(err);
        res.status(500).json({
            message: "Something went wrong. Code: #2212"
        });
    });
});

/**
 * Used to pause active session
 */
router.post('/room/:room_id/session/pause', auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;
    Sessions.pause(room_id, req.session.user.id).then(() => {
        res.json({status: 'OK'});
    }).catch((err) => {
        log_error(err);
        res.status(500).json(err);
    });
});

/**
 * Used to unpause active session
 */
router.post('/room/:room_id/session/unpause', auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;
    Sessions.unpause(room_id, req.session.user.id).then(() => {
        res.json({status: 'OK'});
    }).catch((err) => {
        log_error(err);
        res.status(500).json(err);
    });
});

/**
 * Used to stop room session
 */
router.post('/room/:room_id/session/stop', auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;
    let escaped = req.query.escaped == '1';
    let force = req.query.force == '1';
    let promise = Sessions.session_stop(room_id, escaped, force, req.session.user.id);

    if (promise === true) {
        res.json({status: 'OK'});
    } else if (promise === false) {
        res.status(500).json({'message': "Cannot end session while already ended. Please use force stop"});
    } else {
        promise.then((status) => {
            res.json(status);
        }).catch((err) => {
            res.status(500).json({
                message: "Something went wrong. Code: #2212.1"
            });
            log_error(err);
        });
    }
});

/**
 * Used to get room session information
 */
router.get('/room/:room_id/session', auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;

    Sessions.get_active_session(room_id).then(function (data) {
        res.json(data);
    }).catch(function (err) {
        res.status(500).json({
            message: "Something went wrong. Code: #2212"
        });
    })
});

/**
 * Used to complete a walkthrough for active session
 */
router.post('/room/:room_id/session/walkthrough', auth_middleware, function(req, res, next){
    let room_id = req.params.room_id;
    let walkthrough_param = req.body.walkthrough;

    Sessions.update_active_session(room_id, {
        walkthrough: {
            '$set': walkthrough_param
        }
    }).then(function () {
        let log = {
            user_id: req.session.user.id,
            date: moment.utc().format(),
            action: 'WALKTHROUGH_COMPLETE',
            text: walkthrough_param + 1
        };
        HistoryLog.create(room_id, log);
        send_event_helper('operator.' + room_id, 'log', log);
        res.json({status: 'OK'});
    }).catch(function (err) {
        log_error(err);
        res.status(500).json({
            message: "Something went wrong. Code: #2912"
        });
    });
});


/**
 * Used to post an event that will be delivered to room-clients
 */
router.post('/event/room/:room_id', auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;
    let data = req.body;
    let promises = [];
    let command = data.command;
    let payload = data.data;

    let allowed_commands = ['play_audio', 'stop_audio']; // Commands to allow when session is not running

    Rooms.get_room_by_id(room_id).then(function (room) {
        let log = {
            user_id: req.session.user.id,
            date: moment.utc().format(),
            // action,
            // file,
            // text,
        };

        if (room.session && room.session.state == 'PAUSED') {
            res.status(500).json({message: 'Cannot send commad. Session is paused.'});
            return;
        } else if (!room.session && !(allowed_commands.includes(command)) ) {
            res.status(500).json({message: 'Cannot send commad. Session is not open.'});
            return;
        }

        switch (command) {
            case 'hint':
                payload.end_time = moment.utc().add(room.hint_duration * 60, 'seconds');
                // Create log entry
                log.action = 'HINT';
                if (payload.hint) {
                    log.text = payload.hint;
                }
                if (payload.hint_img) {
                    log.file = payload.hint_img;
                }
                if (room.session.help_requested == 'true' && room.session != false) {
                    Sessions.update_active_session(room_id, {
                        help_requested: {
                            '$set': false
                        },
                        help_date: {
                            $set: moment.utc().add(room.hint_duration * 60, 'seconds').format()
                        }
                    });
                }
                break;
            case 'play_audio':
                log.action = 'AUDIO';
                log.file = payload.file;
                if (payload.asset == 'music' && room.session != false) {
                    Sessions.update_active_session(room_id, {
                        background_music: {
                            '$set': payload.file
                        }
                    });
                }
                break;
            case 'add_time':
                log.action = 'ADD_TIME';
                log.text = payload.seconds;
                if (room.session != false) {
                    promises.push(Sessions.update_active_session(room_id, {extra_seconds: {'$add': payload.seconds}}));
                }
                break;
            case 'alarm':
                log.action = 'ALARM';
                break;
            case 'set_volume_audio':
                if (room.session != false) {
                    let channel_volumes = room.session.channel_volumes;
                    channel_volumes[payload.channel - 1] = payload.volume;
                    Sessions.update_active_session(room_id, {
                        channel_volumes: {
                            '$set': channel_volumes.toString()
                        }
                    });
                }
                break;
            case 'set_master_volume':
                if (room.session != false) {
                    Sessions.update_active_session(room_id, {
                        master_volume: {
                            '$set': payload.volume
                        }
                    });
                }
                break;
        }

        return Promise.all(promises).then(function () {
            if (['hint', 'play_audio', 'add_time', 'alarm'].includes(command)) {
                if (room.session != false) {
                    HistoryLog.create(room_id, log);
                }
                send_event_helper('operator.' + room_id, 'log', log);
            }
            send_event_helper('client.' + room_id, command, payload);
            res.json({status: 'OK'});
        });
    }).catch(function (err) {
        log_error(err);

        res.status(500).json({message: 'Cannot send commad!'});
    });
});

let client_auth_middleware = async function(req, res, next) {
    let ip = req.clientIp.replace('::ffff:', '');

    // USED in development mode
    if (ip === '127.0.0.1' && environment == 'development') {
        req.clientID = -2;
        return next();
    }

    let client = null
    try {
        client = await redis.hgetallAsync('channels:clients:ips:' + ip);
    } catch (error) {
        log_error(error);
        return res.status(500).json({message: 'Internal server error #2.22-1835'});
    }
    if (client !== null) {
        req.clientID = client.slot;
        return next();
    } else {
        return res.status(401).json({message: 'Permission Denied!', status: 401});
    }
};

/**
 * Used to post an event that will be delivered to room-operators
 */
router.post('/event/operator/:room_id', client_auth_middleware, function(req, res, next) {
    let room_id = req.params.room_id;

    let data = req.body;
    let command = data.command;
    let payload = data.data;

    let log = {
        user_id: req.clientID,
        date: moment.utc().format(),
    };

    switch (command) {
        // TODO save to session state
        case 'help_requested':
            send_event_helper('client.' + room_id, command, payload);
            log.action = 'HELP_REQUEST';
            Sessions.update_active_session(room_id, {
                help_requested: {
                    '$set': true
                }
            });
            break;
        default:
            res.status(403).json({message: 'Unknown command'});
    }

    if (['help_requested'].includes(command)) {
        HistoryLog.create(room_id, log);
        send_event_helper('operator.' + room_id, 'log', log);
    }

    send_event_helper('operator.' + room_id, command, payload);
    res.json({status: 'OK'});
});

module.exports = router;
