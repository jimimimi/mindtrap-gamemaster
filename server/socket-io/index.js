module.exports = function (io) {
    const redis = require('../routes/helper/redis').redis;
    const subClient = require('../routes/helper/redis').subClient;
    const types = require('./channels.js');
    const openChannel = require('./redis-channels')(redis).openChannel;
    const namespace = 'channels'
    const log = require('debug')('mindtrap-local:socket-io:log');
    log.log = console.log.bind(console);
    const error = require('debug')('mindtrap-local:socket-io:error');

    subClient.subscribe('server-status');

    io.on('connect', (socket) => {
        // query parameters used for authentication
        let query = socket.handshake.query;

        log('connect\n%O', query);

        if (query === undefined) {
            // query parameters are empty we cannot authenticate the socket
            socket.disconnect(true);
            return;
        }

        let resolved = false;
        for (var key in types) {
            let options = types[key];

            if (query.type === key) {
                openChannel(
                    io,
                    socket,
                    query,
                    options.namespace,
                    options.channel,
                    options.auth,
                    options.onConnect,
                    options.onDisconnect
                );
                resolved = true;
            }
        }

        if (!resolved) {
            error('Unknown connection type', query.type);
            socket.disconnect(true);
        }

    });

    io.on('connection', (socket) => {
        const socketId = socket.id;

        socket.on('new message', function(msg) {
            // log('message: ', msg);
        });

        // log('connection', socketId);

        socket.on('ping', function() {
            // log('ping', socketId);
        });

        socket.on('disconnect', (reason) => {
            log('global disconnect', socket.id);

            // log('disconnect', socketId);
            // TODO: Update redis, client disconnected
        })
    });

    /**
     * Redis message handler
     * used to listen for laravel-backend issued events
     */
    subClient.on('message', function(channel, message) {
        message = JSON.parse(message);
        let ev = message['event'];
        let data = message['data'];
        log(`=====EVENT=====\nChannel: %s\nCommand: "%s"\nData: %O\n===============`, channel, ev, data);


        if (/client\.[0-9]+/g.test(channel)) { // match client channel
            io.to(channel).emit(ev, data);
        } else if (/operator\.[0-9]+/g.test(channel)) {
            io.to(channel).emit(ev, data);
        } else {
            error('Unknown message:', message);
        }
    });

    /**
     * Clean Redis Keys before starting server
     */
    for (var key in types) {
        let options = types[key];
        redis.keysAsync(`${namespace}:${options.channel}s:connections:*`).then((keys) => {
            if (keys.length > 0) {
                redis.del(keys);
            }
        });
    }
}
