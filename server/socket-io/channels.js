const redis = require('../routes/helper/redis').redis;
const subClient = require('../routes/helper/redis').subClient;
const error = require('debug')('mindtrap-local:socket-io:error');

let redis_subscribe_count = {};

let types = {
    'oper': {
        namespace: 'channels',
        channel: 'operator',
        auth: function (socket, query) {
            return new Promise(function(resolve, reject) {
                let username = query.username;
                let array = username.split('-');
                let id = parseInt(array[1]);
                // TODO: authentication


                resolve({
                    id,
                    name: 'This value is hardcoded...',
                    username: username
                });
            });

        },
        onConnect: function (io, socket, authenticated) {
            let channel = 'operator.' + authenticated.id;
            subClient.subscribe(channel);
            if (! channel in redis_subscribe_count) {
                redis_subscribe_count[channel] = 0;
            }
            redis_subscribe_count[channel] += 1;
        },
        onDisconnect: function (io, socket, authenticated) {
            let channel = 'operator.' + authenticated.id;
            redis_subscribe_count[channel] -= 1;
            if (redis_subscribe_count[channel] == 0)
                subClient.unsubscribe('operator.' + authenticated.id);
        }
    },

    'client': {
        namespace: 'channels',
        channel: 'client',
        auth: function (socket, query) {
            return new Promise(function(resolve, reject) {
                if (query.pass !== '123') {
                    error('Disconnect', socket.id ,'invalid password');
                    reject(false);
                }

                // extract query parameters so we can authenticate the client
                let ip = socket.handshake.address.replace(/^.*:/, '');

                if (query.operator_ip) {
                    ip = query.operator_ip;
                }

                redis.hgetallAsync('channels:clients:ips:' + ip)
                    .then(function (obj) {
                        obj.roomId = obj.id;
                        obj.id = obj.id + ':' + obj.slot;
                        resolve(obj);
                    })
                    .catch(function (err) {
                        // error('[ERROR] hgetallAsync', err);
                        reject(false);
                    });
            });

        },
        onConnect: function (io, socket, authenticated) {
            if (!socket.handshake.query.presence_hidden) {
                // inform operators that the client connected
                io.to('operators').emit('client-connect', authenticated.id);
            }
            socket.join('client.' + authenticated.roomId);
            let channel = 'client.' + authenticated.roomId;
            subClient.subscribe(channel);
            if (! channel in redis_subscribe_count) {
                redis_subscribe_count[channel] = 0;
            }
            redis_subscribe_count[channel] += 1;
        },
        onDisconnect: function (io, socket, authenticated) {
            if (!socket.handshake.query.presence_hidden) {
                // inform operators that the client disconnected
                io.to('operators').emit('client-disconnect', authenticated.id);
            }
            let channel = 'client.' + authenticated.roomId;
            redis_subscribe_count[channel] -= 1;
            if (redis_subscribe_count[channel] == 0)
                subClient.unsubscribe('operator.' + authenticated.id);
        }
    }
};

module.exports = types;
