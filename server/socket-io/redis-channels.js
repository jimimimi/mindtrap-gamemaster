    module.exports = function (redis) {
    const error = require('debug')('mindtrap-local:socket-io:error');

    function openChannel(io, socket, query, namespace, channel, authFunc, onConnect, onDisconnect) {
        authFunc(socket, query).then(function (authenticated) {
            // socket is authenticated and we join it in the following channels
            socket.emit('authenticated', authenticated);
            socket.join(channel + '.' + authenticated.id);
            socket.join(channel + 's');

            let redis_props = {
                id: authenticated.id,
                socket: socket.id,
                connected: 'YES'
            }

            if (!query.presence_hidden)
                redis.hmset(namespace + ':' + channel + 's:connections:' + authenticated.id, redis_props);

            if (onConnect !== undefined)
                onConnect(io, socket, authenticated);

            socket.on('disconnect', (reason) => {
                // remove presence record from redis
                let redis_props = {
                    id: authenticated.id,
                    socket: '',
                    connected: 'NO'
                }

                if (!query.presence_hidden)
                    redis.hmset(namespace + ':' + channel + 's:connections:' + authenticated.id, redis_props);

                if (onDisconnect !== undefined)
                    onDisconnect(io, socket, authenticated);
            });
        }).catch(function (err) {
            error('Disconnect', socket.id, 'not authenticated');
            socket.emit('not-authenticated');
            socket.disconnect(true);
        });
    }

    return {
        openChannel
    };
}
