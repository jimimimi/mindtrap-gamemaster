module.exports = {
    secret: 'secret-changeme',
    sync_url: 'http://themindtrap.me/syncstore/store',
    server_sync_url: 'http://themindtrap.me/syncstore/cosmos',
    server_rooms_sync_url: 'http://themindtrap.me/syncrooms/cosmos',
    cors: {
        origin: ['http://localhost:3000', 'http://localhost:8080'],
        credentials: true
    },
    redis: {
        db: 1
    },
    cron: {
        sync_history: '*/5 * * * *', // Every 5 minute
        sync_room_active_sessions: '*/1 * * * *' // Every 1 minute
    },
    port: 3000
}
