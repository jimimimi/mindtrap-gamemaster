# Mindtrap Gamemaster Server

## Folder Structure

* `bin` server excecutable
* `config` application configuration
* `migrations` application migration script
* `routes` application routes and logic
* `public` static files, files come from front-end project
* `socket-io` microframework for current project
* `views` Contain templated views

## Important files

* `app.js` application loader
* `windows-service.js` windows service loader
* `open-api.json` Open API specification file

## About Open API
Load `open-api.json` file at [editor](http://editor.swagger.io/)

## How to setup

1. Copy `example.appconfig.js` -> `appconfig.js`
2. Modify settings
3. `npm run start`

## Client Events

Session

* session-start
* session-stop
* session-clear
* session-paused
* session-unpaused
* add_time

Audio

* play_audio
* pause_audio
* stop_audio
* set_volume_audio
* set_master_volume
* toggle_loop_audio

Help

* hint
* help_requested
* hint_reset
* alarm

Other

* reload
* authenticated
* not-authenticated
* disconnect

## Operator Events

Clients

* client-connect
* client-disconnect

Sessions

* session-start
* session-stop
* session-paused
* session-unpaused
* session-clear
* help_requested
* start
* stop
* log