const axios = require('axios');
import config from './config';

let token = localStorage.getItem('token');

export default axios.create({
    baseURL: config.server_host
});