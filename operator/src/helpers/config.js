import config from '../../appconfig';

export default {
    server_host: config.server_host,
    version: config.version,
    date: config.date,
}
