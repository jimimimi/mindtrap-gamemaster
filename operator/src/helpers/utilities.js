import UIkit from 'uikit';

// helper function used to generate a question modal, return promise (of result)
export function questionModal(title, text, confirmText, confirmColor, cancelText) {
    return UIkit.modal.confirm(`        
        <div class="uk-modal-body">
            <p>
                ${text}
            </p>
        </div>
    `);
}

// creates an error modal
export function errorModal(title, text = '') {
    return UIkit.modal.alert(`
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">
                ${title}
            </h2>
        </div>
        <div class="uk-modal-body">
            <p>
                ${text}
            </p>
        </div>
    `);
}

export default {
    questionModal,
    errorModal
}
