import io from 'socket.io-client';
import config from './config';

export default function (query) {
    return io(config.server_host, { query, reconnection: true, reconnectionAttempts: Infinity,  });
};
