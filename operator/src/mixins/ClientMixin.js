import moment from 'moment';
import _ from 'lodash';
import ServerDate from '@/helpers/serverdate';

export default {
    props: {
        // The initial volume of the room
        initialVolume: {
            type: Number,
            default: 1
        },
        // the number of audio channels
        channels: {
            type: Number,
            default: 3
        },
        // Indicates if this client is an operator
        operator: {
            type: Boolean,
            default: false
        }
    },
    data() {
        return {
            state: {
                connection_status: true, // flag used to determine if client is connected with the server
                hint: '', // hint placeholder
                hint_img: '', // hint image placeholder
                help_indicator: true, // flag used to determine if user allowed to call for help
                help_requested: false, // indicates if client requested help
                help_seconds: -1, // seconds until next help
                alarm_indicator: false, // flags if client is in alarm mode
                game_seconds: 0, // seconds until end of game,
                escape_time: 0, // used to keep the remaining seconds when team escaped
                reset_seconds: 0, // used to keep the seconds until the room resets
                audio_players: [], // used to keep the audio channels
            },
            finate: { // used to keep the master and slave state of the client
                state: 'stopped', // available states ['stopped', 'started']
                sub_state: 'welcome' // available states [ 'stopped' => ['welcome', 'game-over', 'escaped'], 'started' => ['timer', 'hint', 'alarm', 'paused']]
            },
            prev_finate: { // used to store the finate state before changing it in order to restore it
                keep: false,
                state: '',
                sub_state: ''
            },
            intervals: { // Here we hold all the interval pointers of the countdowns
                game_interval: false, // current game countdown
                help_interval: false, // next available help countdown
                reset_interval: false, // client reset countdown
            },
            date_times: {
                game_over: {} // used to hold when then game will finish
            },
            audio_out: false, // used to indicate if client is allowed to play audio
        };
    },
    beforeMount() {
        var self = this;

        // Create the audio_out channels
        for (var channel = 1; channel <= this.channels; channel++) {
            let tmp_channel = channel;
            var player = new Audio();
            var channel_obj = {
                player: player,
                volume: this.initialVolume,
                paused: true,
                loop: false,
                playing: ''
            };
            player.volume = this.initialVolume;

            // Add channels event listeners

            // when track is playing
            player.addEventListener('play', function (e) {
                self.$set(self.state.audio_players[tmp_channel - 1], 'paused', false);

                self.$emit('audio_play', {
                    channel: channel,
                    path: channel_obj.playing
                });
            });

            // when track is paused
            player.addEventListener('pause', function (e) {
                self.$set(self.state.audio_players[tmp_channel - 1], 'paused', true);

                self.$emit('audio_pause', {
                    channel: channel,
                    path: channel_obj.playing
                });
            });

            // when track is ended
            player.addEventListener('ended', function (e) {
                self.$set(self.state.audio_players[tmp_channel - 1], 'paused', true);
                self.$set(self.state.audio_players[tmp_channel - 1], 'playing', '');

                self.$emit('audio_stop', {
                    channel: channel
                });
            });

            // when volume changed
            player.addEventListener('volumechange', function (e) {
                self.$emit('audio_volume_change', {
                    channel: channel,
                    volume: channel_obj.volume
                });
            });

            this.state.audio_players.push(channel_obj);
        }
    },
    methods: {
        // used to set the current state of client
        setFinateState(state, sub_state) {
            this.finate.state = state;
            this.finate.sub_state = sub_state;
        },
        // used to make a backup of the current finate state
        backupState() {
            this.prev_finate.state = this.finate.state;
            this.prev_finate.sub_state = this.finate.sub_state;
            this.prev_finate.keep = true;
        },
        // used to restore the finate state
        restoreState() {
            if (this.prev_finate.keep) {
                this.prev_finate.keep = false;
                this.finate.state = this.prev_finate.state;
                this.finate.sub_state = this.prev_finate.sub_state;
                return true;
            } else {
                return false;
            }
        },
        // used to link static assets with current room configuration
        assetPathHelper(asset, file) {
            return this.$store.state.config.server_host + `/files/${this.roomObj.name}/${asset}/${file}`;
        },
        // used to calculate the total play/game time of the open session
        total_seconds_helper(session) {
            return parseInt(session.total_seconds) + parseInt(session.extra_seconds) + parseInt(session.paused_seconds);
        },
        // used to bind the client to a socket
        registerClientEvents(channel) {
            var self = this;

            channel.on('connect', function() {
                console.log('Connected');
                self.state.connection_status = true;
            });

            channel.on('connect_error', function() {
                console.warn('Connection error!');
                self.state.connection_status = false;
            });

            channel.on('message', function(msg) {
                console.log('Server:', msg);
            });

            channel.on('session-start', self.eventStart);
            channel.on('session-stop', self.eventStop);
            channel.on('session-clear', self.eventSessionClear);
            channel.on('session-paused', self.eventSessionPause);
            channel.on('session-unpaused', self.eventSessionUnpause);
            channel.on('play_audio', self.eventPlayAudio);
            channel.on('pause_audio', self.eventPauseAudio);
            channel.on('stop_audio', self.eventStopAudio);
            channel.on('set_volume_audio', self.eventSetVolumeAudio);
            channel.on('set_master_volume', self.eventSetMasterVolume);
            channel.on('toggle_loop_audio', self.eventToggleLoopAudio);
            channel.on('hint', self.eventHint);
            channel.on('help_requested', self.eventHelpRequested);
            channel.on('hint_reset', self.eventHintReset);
            channel.on('alarm', self.eventAlarm);
            channel.on('add_time', self.eventAddTime);
            channel.on('reload', self.eventReload);
        },
        // used to format the seconds in format
        format_seconds(format, seconds) {
            return moment.utc("2015-01-01").startOf('day')
                    .seconds(seconds)
                    .format(format);
        },
        // used to format seconds into HH:mm:ss
        format_time(seconds) {
            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }

            let time = moment.utc("2015-01-01").startOf('day').seconds(seconds);
            let hour = new String(time.hour() * 60 + time.minutes());
            let minutes = new String(time.seconds());
            hour = zeroPad(hour, 2);
            minutes = zeroPad(minutes, 2);
            return `${hour}:${minutes}`;
        },
        // used to safely stop an interval
        clearNamedInterval(name) {
            clearInterval(this.intervals[name]);
            this.intervals[name] = undefined;
        },
        // used to start the room reset countdown
        start_reset_interval(session) {
            this.clearNamedInterval('reset_interval');

            let self = this;
            let reset_end_time = moment.utc(session.stop_date).add(3 * 60, 'seconds');

            this.intervals.reset_interval = setInterval(() => {
                let seconds = reset_end_time.diff(moment( ServerDate.now() ).utc(), 'seconds');

                self.state.reset_seconds = seconds;
                if (seconds <= 0) {
                    self.state.reset_seconds = 0;
                    self.clearNamedInterval('reset_interval');
                }
            }, 250)
        },
        // used to start the game countdown
        start_game_interval() {
            let self = this;

            // function is used to play audio once every second
            let clock_tick = _.once(function() {
                self.eventPlayAudio({
                    channel: 3,
                    asset: 'sound',
                    file: 'countdown_30_seconds.mp3'
                });
            })

            this.clearNamedInterval('game_interval');
            this.intervals.game_interval = setInterval(function () {
                let seconds = self.date_times.game_over.diff(moment( ServerDate.now() ).utc(), 'seconds');
                self.state.game_seconds = seconds;

                if (seconds <= 30) {
                    // play clock_tick.mp3 the last 30 seconds of the game
                    clock_tick();
                }

                if (seconds < 0) {
                    // when game time is ended we set the state to game-over unless operator states otherwise
                    if (self.finate.state == 'started') {
                        self.setFinateState('stopped', 'game-over');
                    }
                    self.state.game_seconds = 0;
                    self.clearNamedInterval('game_interval');
                }
            }, 250);
        },
        // function is used to restore the state of the session
        initialize_state(room) {
            if (room.session) {

                if (room.session.state == 'RUNNING') {
                    // If audio out is from gamemaster do not auto-play audio
                    if (room.audio_out != 'GAMEMASTER')
                        this.startBackgroundMusic(room);

                    // initialize if help has been requested and not answered
                    this.state.help_requested = room.session.help_requested == 'true';

                    // If Gamemaster given hint start hint_available countdown
                    if (room.session.help_date != 'false') {
                        this.start_hint_available_countdown(room.session.help_date);
                    }

                    // start game countdown
                    this.eventStart({
                        start_date: room.session.start_date,
                        total_seconds: this.total_seconds_helper(room.session)
                    });
                } else if (room.session.state == 'PAUSED') {
                    // start game countdown
                    this.eventStart({
                        start_date: room.session.start_date,
                        total_seconds: this.total_seconds_helper(room.session)
                    });

                    // pause the game
                    this.eventSessionPause(room.session);
                } else { // game state ended
                    // start game countdown
                    this.eventStart({
                        start_date: room.session.start_date,
                        total_seconds: this.total_seconds_helper(room.session)
                    });
                    // game ended
                    this.eventStop({
                        escaped: room.session.state == 'ESCAPED',
                        session: room.session
                    });
                    // start reset countdown
                    this.start_reset_interval(room.session);
                }

            }
        },
        // used to start the background music of room
        startBackgroundMusic(room) {
            let data = {};

            data.channel = 1;
            data.asset = 'music';
            if (room.session) {
                data.file = room.session.background_music;
            } else {
                data.file = 'background.mp3';
            }

            this.loopHelper(data.channel, true);
            this.eventPlayAudio(data);
        },
        // used to stop the background music
        stopBackgroundMusic() {
            let data = {};
            data.channel = 1;

            this.eventStopAudio(data);
        },

        // Socket - IO events
        /**
         * session-start handler
         * @param {Object} data event data
         */
        eventStart(data) {
            console.warn('start', data);
            var self = this;

            this.setFinateState('started', 'timer');

            let time = moment.utc(data.start_date).add(data.total_seconds, 'seconds');

            // set when the game is planned to end
            this.$set(this.date_times, 'game_over', time);

            this.start_game_interval();
            // start background music only when audio_out is not GAMEMASTER
            if (this.roomObj.audio_out != 'GAMEMASTER')
                this.startBackgroundMusic(this.roomObj);
        },
        /**
         * session-stop handler
         * @param {Object} data event data
         */
        eventStop(data) {
            console.warn('stop');
            let self = this;
            let escaped = data.escaped;
            let session = data.session;

            this.stopBackgroundMusic();
            this.clearNamedInterval('game_interval');
            this.clearNamedInterval('help_interval');

            // calculate when the game ended (in order to display escape time)
            let end_time = moment.utc(session.start_date).add(this.total_seconds_helper(session), 'seconds');
            let stop = moment.utc(session.stop_date);
            let diff = end_time.diff(stop, 'seconds');
            this.state.game_seconds = diff >= 0 ? diff : 0;

            if (escaped) {
                this.state.escape_time = end_time.diff(stop, 'seconds');
                this.setFinateState('stopped', 'escaped');
            } else {
                this.setFinateState('stopped', 'game-over');
            }

            this.start_reset_interval(session);
        },
        /**
         * session-paused handler
         * @param {Object} session session data
         */
        eventSessionPause(session) {
            if (this.finate.state != 'started') {
                return;
            }
            console.warn('pause');
            this.backupState();
            this.setFinateState('started', 'paused');
            let end_time = moment.utc(session.start_date).add(this.total_seconds_helper(session), 'seconds');
            let pause_time = moment.utc(session.paused_date);
            this.state.game_seconds = end_time.diff(pause_time, 'seconds');

            this.clearNamedInterval('game_interval');
            this.clearNamedInterval('help_interval');
        },
        /**
         * session-unpaused handler
         * @param {Object} session session data
         */
        eventSessionUnpause(session) {
            if (this.finate.state != 'started') {
                return;
            }
            console.warn('unpause');

            if (! this.restoreState()) {
                this.setFinateState('started', 'timer');
            }

            let total_seconds = this.total_seconds_helper(session);

            let time = moment.utc(session.start_date).add(total_seconds, 'seconds');

            this.$set(this.date_times, 'game_over', time);

            this.start_game_interval();
            if (session.help_date != 'false') {
                this.start_hint_available_countdown(session.help_date);
            }
        },
        /**
         * session-clear handler
         */
        eventSessionClear() {
            this.clearNamedInterval('reset_interval');

            this.setFinateState('stopped', 'welcome');
            this.state.game_seconds = 0;
            this.state.escape_time = 0;
            this.state.help_seconds = -1;
            this.state.help_indicator = true;
            this.state.help_requested = false;
            this.state.hint = '';
            this.state.hint_img = '';
        },
        /**
         * play_audio handler
         * @param {Object} data event data
         */
        eventPlayAudio(data) {
            // console.warn('Event play_audio');
            let channel = data.channel;
            let file = data.file;
            let asset = data.asset;
            let channel_obj = this.state.audio_players[channel - 1];
            let player = channel_obj.player;
            if (file != channel_obj.playing) {
                player.src = this.assetPathHelper(asset, file);
                this.$set(channel_obj, 'playing', file);
            }
            player.play();
        },
        /**
         * pause_audio handler
         * @param {Object} data event data
         */
        eventPauseAudio(data) {
            // console.warn('Event pause_audio');
            let channel = data.channel;
            let channel_obj = this.state.audio_players[channel - 1];
            let player = channel_obj.player;
            player.pause();
        },
        /**
         * stop_audio handler
         * @param {Object} data event data
         */
        eventStopAudio(data) {
            // console.warn('Event stop_audio');
            let channel = data.channel;
            let channel_obj = this.state.audio_players[channel - 1];
            let player = channel_obj.player;
            player.pause();
            this.$set(channel_obj, 'playing', '');
        },
        /**
         * Used to set a channel in loop mode
         * @param {Integer} channel
         * @param {Object} state
         */
        loopHelper(channel, state) {
            let channel_obj = this.state.audio_players[channel - 1];
            let player = channel_obj.player;
            this.$set(channel_obj, 'loop', state);
            player.loop = channel_obj.loop;
            this.$emit('audio_loop', {
                channel: channel,
                loop: player.loop
            });
        },
        /**
         * toggle_loop_audio handler
         * @param {Object} data event data
         */
        eventToggleLoopAudio(data) {
            // console.warn('Event toggle_loop_audio');
            let channel = data.channel;
            let channel_obj = this.state.audio_players[channel - 1];
            let player = channel_obj.player;
            this.$set(channel_obj, 'loop', !channel_obj.loop);
            player.loop = channel_obj.loop;
            this.$emit('audio_loop', {
                channel: channel,
                loop: player.loop
            });
        },
        /**
         * set_volume_audio handler
         * @param {Object} data event data
         * @todo implement operator mixer
         */
        eventSetVolumeAudio(data) {

            let channel = data.channel;
            let volume = data.volume;
            if (this.roomObj.session) {
                this.$set(this.roomObj.session.channel_volumes, channel - 1 , volume);
            }
            if ( ! this.audio_out) {
                return;
            }
            // console.warn('Event set_volume_audio');
            let channel_obj = this.state.audio_players[channel - 1];
            let player = channel_obj.player;
            this.$set(channel_obj, 'volume', volume);
            let master = this.roomObj.session ? this.roomObj.session.master_volume : 1;

            player.volume = volume * master;
        },
        /**
         * set_master_volume handler
         * @param {Object} data event data
         * @todo implement operator mixer
         */
        eventSetMasterVolume(data) {
            let volume = data.volume;

            if (this.roomObj.session) {
                this.$set(this.roomObj.session, 'master_volume' , volume);
            }

            if ( ! this.audio_out) {
                return;
            }
            let array = this.state.audio_players;
            for (let index = 0; index < array.length; index++) {
                let channel_obj = array[index];
                let player = channel_obj.player;
                let volume_value = channel_obj.volume * volume;
                player.volume = volume_value;
            }
        },
        /**
         * Used to display the next available hint countdown
         * @param {String} end_time expected time for countdown to stop
         * @param {Boolean} event Used to indicate if method called from an event or not
         */
        start_hint_available_countdown(end_time, event = false) {
            let self = this;
            let help_end_time = moment.utc(end_time);
            let next_available_hint = help_end_time.diff(moment( ServerDate.now() ).utc(), 'seconds');

            if ( (next_available_hint > 0 && !event) || (next_available_hint > 0 && self.state.help_indicator && self.state.help_requested) ) {
                self.$set(self.state, 'help_indicator', false);
                self.$set(self.state, 'help_requested', false);

                self.clearNamedInterval('help_interval');
                self.intervals.help_interval = setInterval(function () {
                    let seconds = help_end_time.diff(moment( ServerDate.now() ).utc(), 'seconds');
                    self.state.help_seconds = seconds;
                    if (seconds < 0) {
                        self.$set(self.state, 'help_indicator', true);
                        self.clearNamedInterval('help_interval');
                    }
                }, 250);
            }
        },
        /**
         * hint handler
         * @param {Object} data event data
         */
        eventHint(data) {
            if (this.finate.state != 'started') {
                return;
            }

            this.setFinateState('started', 'hint');

            if (data.hint)
                this.$set(this.state, 'hint', data.hint);
            else
                this.$set(this.state, 'hint', '');

            if (data.hint_img)
                this.$set(this.state, 'hint_img', this.assetPathHelper('images', data.hint_img));
            else
                this.$set(this.state, 'hint_img', '');

            this.eventPlayAudio({
                asset: 'sound',
                file: 'hint.mp3',
                channel: 3
            });

            this.start_hint_available_countdown(data.end_time, true);
        },
        /**
         * help_requested handler
         */
        eventHelpRequested() {
            this.state.help_requested = true;
        },
        /**
         * hint_reset handler
         */
        eventHintReset() {
            if (this.finate.state != 'started') {
                return;
            }
            this.setFinateState('started', 'timer');
            this.state.hint = '';
            this.state.hint_img = '';
        },
        /**
         * alarm handler
         */
        eventAlarm() {
            var self = this;
            if (this.finate.state != 'started') {
                return;
            }
            this.backupState();
            this.setFinateState('started', 'alarm');

            this.eventPlayAudio({
                asset: 'sound',
                file: 'alarm.mp3',
                channel: 3
            });

            // State of emergency
            self.state.alarm_indicator = true;
            // Stop state of emergency after 5 seconds
            setTimeout(function () {
                self.restoreState();
            }, 5000);
        },
        /**
         * add_time handler
         * @param {Object} payload event data
         */
        eventAddTime(payload) {
            if (this.state.game_seconds > 0) {
                let datetime = moment.utc(this.date_times.game_over).add(payload.seconds, 'seconds');
                this.$set(this.date_times, 'game_over', datetime);
            }
        },
        /**
         * reload handler
         * @param {Object} payload event data
         */
        eventReload(payload) {
            if (this.operator) {
                return;
            }
            if (this.clientIp == payload.client_ip) {
                location.reload(true)
            }
        }
    },
    beforeDestroy() {
        this.clearNamedInterval('game_interval');
        this.clearNamedInterval('help_interval');
    }
}
