/**
 * Mixin for DynamicContant components
 * Used from Client to load custom client template
 * Originates from CafePro cloud
 */
import Vue from 'vue';
import axios from '@/helpers/axios';

export default {
    props: {
        /**
         * @param {String} contentProp location to dynamic html location
         */
        contentProp: {
            type: String,
            default: undefined
        },
    },
    data() {
        return {
            'template': null,
            content: this.contentProp,
            title: '',
            variables: {},
        }
    },
    render: function(createElement) {
        if (!this.template) {
            var e = createElement('div', {
                class: "wait-panel"
            }, [createElement('div', {
                class: "clock-spinner"
            })]);

            return e;
        } else {
            return this.template();
        }
    },
    mounted() {
        var self = this;

        if (self.content !== undefined)
            self.renderHelperFunction();

        if (self.onMounted) {
            self.onMounted();
        }
    },
    methods: {
        /**
         * Used to compile html code to Vue render code
         * @param {String} template_html
         */
        htmlRenderHelper(template_html) {
            var result = Vue.compile(template_html);

            this.template = result.render;

            // staticRenderFns belong into $options,
            // appearantly
            this.$options.staticRenderFns = []

            // clean the cache of static elements
            // this is a cache of the results from the staticRenderFns
            this._staticTrees = []

            // Fill it with the new staticRenderFns
            for (var i in result.staticRenderFns) {
                //staticRenderFns.push(res.staticRenderFns[i]);
                this.$options.staticRenderFns.push(result.staticRenderFns[i]);
            }
        },
        renderHelperFunction() {
            var self = this;
            axios.get(this.content).then(function (response) {
                self.htmlRenderHelper(response.data)
            }).catch((error) => {
                // TODO error handler
                console.error(error)
            });
        }
    }
}
