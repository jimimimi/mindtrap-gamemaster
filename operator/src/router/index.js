/**
 * Application Routes configuration
 * https://router.vuejs.org/
 * for official documetation
 */
import Vue from 'vue'
import Router from 'vue-router'

import OperatorsPage from '@/components/OperatorPage'
import Changelog from '@/components/Changelog'
import RoomsGrid from '@/components/operator/RoomsGrid'
import OperatorClient from '@/components/operator/OperatorClient'

import Client from '@/components/Client'

import LoginPage from '@/components/LoginPage'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            component: OperatorsPage,
            children: [{
                path: '',
                name: 'RoomsGrid',
                component: RoomsGrid
            }, {
                path: 'room/:room_id/panel',
                name: 'OperatorClient',
                component: OperatorClient,
                props: {
                    operator: true,
                    initialVolume: 0
                }
            }, {
                path: 'changelog',
                name: 'Changelog',
                component: Changelog
            }]
        },
        {
            path: '/client/:client_ip',
            name: 'Client',
            component: Client,
            props(route) {
                return {
                    clientIp: route.params.client_ip,
                    initialVolume: 0
                };
            }
        },
        {
            path: '/login',
            name: 'login',
            component: LoginPage
        }
    ]
})
