/**
 * Application Global State
 * https://vuex.vuejs.org/
 * for official documetation
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        connection_status: true,
        loaded: false,
        rooms: {},
        config: {}
    },
    mutations: {
        setRooms(state, rooms) {
            for (var i = 0; i < rooms.length; i++) {
                let room = rooms[i];
                Vue.set(state.rooms, room.id, room);
            }
        },
        disconnectAll(state) {
            for (var room_id in state.rooms) {
                if (state.rooms.hasOwnProperty(room_id)) {
                    for (var slot = 1; slot <= 4; slot++) {
                        if (slot in state.rooms[room_id].clients) {
                            state.rooms[room_id].clients[slot].connected = false;
                        }
                    }
                }
            }
        },
        updateConnectionStatus(state, payload) {
            let room_id = payload.room_id;
            let slot = payload.slot;
            let status = payload.status;
            if (room_id in state.rooms) {
                if (slot in state.rooms[room_id].clients) {
                    Vue.set(state.rooms[room_id].clients[slot], 'connected', status);
                } else {
                    console.warn('Invalid slot id:', slot,'for room id:', room_id);
                }
            } else {
                console.warn('Invalid room id:', room_id);
            }
        },
        updateRoomSession(state, payload) {
            let room_id = payload.room_id;
            let session = payload.session;
            if (room_id in state.rooms) {
                if (state.rooms[room_id].session){
                    state.rooms[room_id].session = Object.assign({}, state.rooms[room_id].session, session);
                } else {
                    state.rooms[room_id].session = session;
                }
            } else {
                console.warn('Invalid room id:', room_id);
            }
        },
        clearRoomSession(state, payload) {
            let room_id = payload.room_id;

            if (room_id in state.rooms) {
                state.rooms[room_id].session = false;
            } else {
                console.warn('Invalid room id:', room_id);
            }
        },
        load(state) {
            state.loaded = true;
        },
        setConnectionStatus(state, status) {
            state.connection_status = status;
        },
        setConfig(state, payload) {
            Vue.set(state, 'config', payload);
        }
    }
});
