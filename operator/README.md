# Mindtrap Frontend

Mindtrap Gamemaster Frontend

## Folder Structure

[Official documentation](http://vuejs-templates.github.io/webpack/structure.html)

## Important files

* `appconfig.example.js` application supported configuration, copy it
and rename it to `appconfig.js` and modify settings for production

* `config.json` is similar to `appconfig.js` but it is placed in the root folder of the application to provide production dynamic configuration

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
